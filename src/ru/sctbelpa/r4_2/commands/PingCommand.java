package ru.sctbelpa.r4_2.commands;

import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;

public class PingCommand extends Command {

	public PingCommand(Handler h, myActivtyInterface callbackRessiver,
			BluetoothSocket socket) {
		super(h, callbackRessiver, socket);
	}

	@Override
	protected void onSuccess() {
		
	}

	@Override
	protected void parseAnsver(String ansver) {
		if (ansver.contains("pong"))
			onSuccess();
		else 
			onFail(0xff);
	}

	@Override
	public String command() {
		return "ping";
	}	
}
