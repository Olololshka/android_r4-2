package ru.sctbelpa.r4_2.commands;

import java.util.List;
import java.util.Locale;

import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.widget.Toast;

import java.util.Formatter;

import ru.sctbelpa.r4_2.R;
import ru.sctbelpa.r4_2.R4_2App;
import ru.sctbelpa.r4_2.coefficient.Coefficient;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;

public class WriteCoeffsCommand extends Command {

	public static final String writeFormat = 
			"%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";
	private static final int ERR_IN_RESSIVER = 0xff;

	private final String CoeffsStr;

	public WriteCoeffsCommand(Handler h, myActivtyInterface callbackRessiver,
			BluetoothSocket socket, List<Coefficient> coeffs) {
		super(h, callbackRessiver, socket);
		StringBuilder sb = new StringBuilder();
		Formatter formatter = new Formatter(sb, Locale.US);
		formatter.format(writeFormat,
				coeffs.get(0).formatOutput(),
				coeffs.get(1).formatOutput(),
				coeffs.get(2).formatOutput(),
				coeffs.get(3).formatOutput(),
				coeffs.get(4).formatOutput(),
				coeffs.get(5).formatOutput(),
				coeffs.get(6).formatOutput(),
				coeffs.get(7).formatOutput(),
				coeffs.get(8).formatOutput(),
				coeffs.get(9).formatOutput(),
				coeffs.get(10).formatOutput(),
				coeffs.get(11).formatOutput(),
				coeffs.get(12).formatOutput(),
				coeffs.get(13).formatOutput(),
				coeffs.get(14).formatOutput());
		CoeffsStr = sb.toString();
		formatter.close();
	}

	@Override
	protected void onSuccess() {
		h.post(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(R4_2App.getAppContext(), 
						R.string.write_coeffs_success, Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	protected void parseAnsver(String ansver) {
		if (ansver.contains("[Accepted]"))
			onSuccess();
		else
			onFail(ERR_IN_RESSIVER);
	}

	@Override
	public String command() {
		return "coeffs " + CoeffsStr;
	}

	@Override
	protected void onFail(int code) {
		if (code != ERR_CONNECT)
			h.post(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(R4_2App.getAppContext(), 
							R.string.write_coeffs_failed, Toast.LENGTH_LONG).show();
				}
			});
		else
			super.onFail(code);
	}

}
