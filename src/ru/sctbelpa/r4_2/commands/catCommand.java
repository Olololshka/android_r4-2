package ru.sctbelpa.r4_2.commands;

import ru.sctbelpa.r4_2.FileDetailFragment;
import ru.sctbelpa.r4_2.R;
import ru.sctbelpa.r4_2.R4_2App;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.widget.Toast;

public class catCommand extends Command {

	private String filename;
	private String result;

	private static final int ERR_REMOTE = 0x30;
	private static final int ERR_EMPTY_FILE = 0x40;

	public catCommand(Handler h, myActivtyInterface callbackRessiver,
			BluetoothSocket socket, String filename) {
		super(h, callbackRessiver, socket);
		this.filename = filename;
		this.timeoutMS = 150;
	}

	@Override
	protected void onSuccess() {
		if (callbackRessiver instanceof FileDetailFragment)
		{
			h.post(new Runnable() {
				
				@Override
				public void run() {
					((FileDetailFragment) callbackRessiver).SetData(result);
				}
			});
		}
	}

	@Override
	protected void parseAnsver(String ansver) {
		if (ansver.contains("Error") || ansver.contains("error") ||
				ansver.contains("Failed"))
			onFail(ERR_REMOTE);
		else if (ansver.contains("[Empty]"))
			onFail(ERR_EMPTY_FILE);
		else
		{
			result = ansver.trim();
			onSuccess();
		}
	}

	@Override
	public String command() {
		return "cat " + filename;
	}

	@Override
	protected void onFail(int code) {
		
		if (callbackRessiver instanceof FileDetailFragment)
		{
			h.post(new Runnable() {
				
				@Override
				public void run() {
					((FileDetailFragment) callbackRessiver).SwitchViewMode(
							FileDetailFragment.SHOW_EMPTY);
				}
			});
		}
		
		switch (code)
		{
		case ERR_REMOTE:
			h.post(new Runnable() {
				
				@Override
				public void run() {
					Toast.makeText(R4_2App.getAppContext(), 
							R4_2App.getAppContext().getResources().getString(
									R.string.Failed_to_open_remote) + " " + filename,
							Toast.LENGTH_SHORT).show();
				}
			});
			break;
		default:
			super.onFail(code);
		}
	}

}
