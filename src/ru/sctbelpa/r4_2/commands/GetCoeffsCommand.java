package ru.sctbelpa.r4_2.commands;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.sctbelpa.r4_2.MainActivity;
import ru.sctbelpa.r4_2.R;
import ru.sctbelpa.r4_2.R4_2App;
import ru.sctbelpa.r4_2.coefficient.Coefficient;
import ru.sctbelpa.r4_2.coefficient.FloatCoefficient;
import ru.sctbelpa.r4_2.coefficient.IntCoefficient;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.widget.Toast;

public class GetCoeffsCommand extends Command {

	private static final String pattern =
			// "%x;%x;%x;%x; %x;%x;%x;%x; %x;%x;%x;%x; %x;%d;%d";
			"([0-9A-Fa-f]*);([0-9A-Fa-f]*);([0-9A-Fa-f]*);([0-9A-Fa-f]*);" + 
			"([0-9A-Fa-f]*);([0-9A-Fa-f]*);([0-9A-Fa-f]*);([0-9A-Fa-f]*);" + 
			"([0-9A-Fa-f]*);([0-9A-Fa-f]*);([0-9A-Fa-f]*);([0-9A-Fa-f]*);" +
			"([0-9A-Fa-f]*);([0-9]*);([0-9]*)";

	private static final int ERR_PARCE = 0xff;

	public static final String coeffsNames[] = {
			"A[1]", "A[2]", "A[3]", "A[4]", "A[5]", "A[6]", "Fp0", "Ft0",
			"T0", "C[1]", "C[2]", "C[3]", "F0"};
	private int coeffsAddrs[] = {7, 9, 0xb, 0xd, 0xf, 0x11, 0x13, 0x15, 
			0x17, 0x19, 0x1b, 0x1d, 0x1f,
			3, 4};

	private final Pattern p;
	private final List<Coefficient> result;

	public GetCoeffsCommand(Handler h, myActivtyInterface callbackRessiver,
			BluetoothSocket socket) {
		super(h, callbackRessiver, socket);
		p = Pattern.compile(pattern);
		result = new LinkedList<Coefficient>();
	}

	@Override
	protected void onSuccess() {
		h.post(new Runnable() {
			
			@Override
			public void run() {
				if (callbackRessiver instanceof MainActivity)
					((MainActivity)callbackRessiver).showCoeffsEditDialog(result);
			}
		});
	}

	@Override
	protected void parseAnsver(String ansver) {
		
		Matcher m = p.matcher(ansver);
		if (m.find())
		{
			Coefficient c;
			for (int i = 1; i < 14; ++i)
			{
				c = new FloatCoefficient(coeffsNames[i - 1], m.group(i));
				c.setAddress(coeffsAddrs[i - 1]);
				result.add(c);
			}

			c = new IntCoefficient(
					R.string.T_mp, m.group(14), R.string.ms);
			c.setAddress(coeffsAddrs[13]);
			result.add(c);
			c = new IntCoefficient(
					R.string.T_mt, m.group(15), R.string.ms);
			c.setAddress(coeffsAddrs[14]);
			result.add(c);

			onSuccess();
			return;
		}
		onFail(ERR_PARCE);
	}

	@Override
	public String command() {
		return "coeffs";
	}

	@Override
	protected void onFail(int code) {
		if (code != ERR_CONNECT)
		{
			h.post(new Runnable() {
				
				@Override
				public void run() {
					Toast.makeText(
							R4_2App.getAppContext(), R.string.readCoeffs_fail, Toast.LENGTH_LONG)
							.show();
				}
			});
		}
		else
			super.onFail(code);
	}
}
