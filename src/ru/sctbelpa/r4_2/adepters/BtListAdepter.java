package ru.sctbelpa.r4_2.adepters;

import java.util.List;

import ru.sctbelpa.r4_2.R;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BtListAdepter extends BaseAdapter {

	private static final String ApplicatableDevNameTemplate = "SCTB_R4-2";
	private Context mContext;
    private LayoutInflater inflater;
    private List<BluetoothDevice> mDevices;
    
    public BtListAdepter(Context context, List<BluetoothDevice> Devices) {
		mContext = context;
		inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mDevices = Devices;
	}
	
	@Override
	public int getCount() {
		return mDevices.size();
	}

	@Override
	public Object getItem(int arg0) {
		if (arg0 < mDevices.size())
			return mDevices.get(arg0);
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View container, ViewGroup parent) {
		
		View view = (container == null) ? 
				(inflater.inflate(R.layout.bt_list_item, parent, false)) : container;
		
		BluetoothDevice d = mDevices.get(position);
		
		String name = d.getName();
		
		ImageView icon = (ImageView)view.findViewById(R.id.device_icon);
		if (name.contains(ApplicatableDevNameTemplate))
			icon.setImageResource(R.drawable.ic_launcher);
		else
			icon.setImageResource(R.drawable.bt);
		
		((TextView)view.findViewById(R.id.name)).setText(name);
		((TextView)view.findViewById(R.id.mac)).setText(d.getAddress());
		return view;
	}
}
