package ru.sctbelpa.r4_2;

import java.util.List;

import ru.sctbelpa.r4_2.FsModel.FSContent;
import ru.sctbelpa.r4_2.FsModel.FSContent.FsObject;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;

/**
 * An activity representing a list of Files. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link FileDetailActivity} representing item details. On tablets, the
 * activity presents the list of items and item details side-by-side using two
 * vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link FileListFragment} and the item details (if present) is a
 * {@link FileDetailFragment}.
 * <p>
 * This activity also implements the required {@link FileListFragment.Callbacks}
 * interface to listen for item selections.
 */
public class FileListActivity extends FragmentActivity implements
FileListFragment.Callbacks, myActivtyInterface {

	public static final int ACTIVITY_ID = 1;
	private static final String  DirectorySaveKey = "ru.sctbelpa.meteostation_service.currentPath";
	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */
	private boolean mTwoPane;
	private Handler h;

	private String directory; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_list);

		if (findViewById(R.id.file_detail_container) != null) {
			// The detail container view will be present only in the
			// large-screen layouts (res/values-large and
			// res/values-sw600dp). If this view is present, then the
			// activity should be in two-pane mode.
			mTwoPane = true;

			// In two-pane mode, list items should be given the
			// 'activated' state when touched.
			((FileListFragment) getSupportFragmentManager().findFragmentById(
					R.id.file_list)).setActivateOnItemClick(true);
		}

		if (savedInstanceState!= null)
			directory = savedInstanceState.getString(DirectorySaveKey, "/");
		else 
			directory = "/";

		h = new Handler();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString(DirectorySaveKey, directory);
		super.onSaveInstanceState(outState);
	}
	
	public void lsResult(List<FsObject> files)
	{
		FSContent.clearItems();
		FSContent.addItem(new FsObject("../"));
		FSContent.addItems(files);
		FSContent.adeptor.notifyDataSetChanged();
	}

	public void RereadDirectory()
	{
		R4_2App.execCommand(
				R4_2App.buildCommand(
						R4_2App.CMD_LS, FileListActivity.this, "."));
	}

	private long back_pressed;

	@Override
	public void onBackPressed() 
	{
		if (back_pressed + 1000 > System.currentTimeMillis()) 
			super.onBackPressed();
		else 
		{
			if (directory.equals("/"))
				super.onBackPressed();
			else
				dirmoveUp();
		}
		back_pressed = System.currentTimeMillis();
	}

	private void dirmoveUp()
	{
		int slashIndex = directory.lastIndexOf('/');
		if (slashIndex == 0)
		{
			dirResetPath();
			return;
		}
		
		directory = directory.substring(0, slashIndex);

		R4_2App.execCommand(R4_2App.buildCommand(
				R4_2App.CMD_CD, FileListActivity.this, ".."));
	}
	
	private void dirStepInto(String Dirname)
	{
		R4_2App.execCommand(R4_2App.buildCommand(
				R4_2App.CMD_CD, FileListActivity.this, Dirname));
	}
	
	private void dirResetPath()
	{
		directory = "/";
		
		R4_2App.execCommand(R4_2App.buildCommand(
				R4_2App.CMD_CD, FileListActivity.this, "/"));
	}

	@Override
	protected void onResume() {
		R4_2App.ActivityChangedStatus(FileListActivity.this, true);
		if (R4_2App.isBtConnected())
			RereadDirectory();
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		R4_2App.ActivityChangedStatus(FileListActivity.this, false);
		super.onPause();
	}
	
	/**
	 * Callback method from {@link FileListFragment.Callbacks} indicating that
	 * the item with the given ID was selected.
	 */
	@Override
	public void onItemSelected(String id) {
		boolean isFileSelected = false;
		try
		{
			if (FSContent.ITEM_MAP.get(id).isFolder)
			{
				if (id.equals(".."))
					dirmoveUp();
				else
					dirStepInto(id);
			}
			else
				isFileSelected = true;
		}
		catch (NullPointerException e)
		{
			dirResetPath();
		}

		if (isFileSelected)
		{
			if (mTwoPane) {
				// In two-pane mode, show the detail view in this activity by
				// adding or replacing the detail fragment using a
				// fragment transaction.
				Bundle arguments = new Bundle();
				arguments.putString(FileDetailFragment.ARG_ITEM_NAME, id);
				//arguments.putString(FileDetailFragment.DIRECTORY_NAME, directory);
				FileDetailFragment fragment = new FileDetailFragment();
				fragment.setArguments(arguments);
				getSupportFragmentManager().beginTransaction()
				.replace(R.id.file_detail_container, fragment).commit();

			} else {
				// In single-pane mode, simply start the detail activity
				// for the selected item ID.
				Intent detailIntent = new Intent(this, FileDetailActivity.class);
				detailIntent.putExtra(FileDetailFragment.ARG_ITEM_NAME, id);
				//detailIntent.putExtra(FileDetailFragment.DIRECTORY_NAME, directory);
				startActivity(detailIntent);
			}
		}
	}

	@Override
	public Handler getHandler() {
		return h;
	}

	@Override
	public Integer getID() {
		return Integer.valueOf(ACTIVITY_ID);
	}

	@Override
	public Context getContext() {
		// TODO Auto-generated method stub
		return this;
	}
}
