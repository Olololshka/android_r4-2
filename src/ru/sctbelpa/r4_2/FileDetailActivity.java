package ru.sctbelpa.r4_2;

import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;

/**
 * An activity representing a single File detail screen. This activity is only
 * used on handset devices. On tablet-size devices, item details are presented
 * side-by-side with a list of items in a {@link FileListActivity}.
 * <p>
 * This activity is mostly just a 'shell' activity containing nothing more than
 * a {@link FileDetailFragment}.
 */
public class FileDetailActivity extends FragmentActivity implements myActivtyInterface {
	
	private Handler h;
	private static final int ID = 5;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_detail);
		
		h = new Handler();

		// savedInstanceState is non-null when there is fragment state
		// saved from previous configurations of this activity
		// (e.g. when rotating the screen from portrait to landscape).
		// In this case, the fragment will automatically be re-added
		// to its container so we don't need to manually add it.
		// For more information, see the Fragments API guide at:
		//
		// http://developer.android.com/guide/components/fragments.html
		//
		if (savedInstanceState == null) {
			// Create the detail fragment and add it to the activity
			// using a fragment transaction.
			Bundle arguments = getIntent().getExtras();
			FileDetailFragment fragment = new FileDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction()
					.add(R.id.file_detail_container, fragment).commit();
		}
	}
	
	@Override
	protected void onResume() {
		R4_2App.ActivityChangedStatus(FileDetailActivity.this, true);
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		R4_2App.ActivityChangedStatus(FileDetailActivity.this, false);
		super.onPause();
	}

	@Override
	public Handler getHandler() {
		return h;
	}

	@Override
	public Integer getID() {
		return ID;
	}

	@Override
	public Context getContext() {
		return this;
	}
}
