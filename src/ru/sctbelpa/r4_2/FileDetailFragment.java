package ru.sctbelpa.r4_2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.android.openfiledialog.OpenFileDialog;
import ru.sctbelpa.r4_2.FsModel.FSContent;
import ru.sctbelpa.r4_2.dialogs.ExportCoeffsDialog;
import ru.sctbelpa.r4_2.halpers.Dataset;
import ru.sctbelpa.r4_2.halpers.graph;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A fragment representing a single File detail screen. This fragment is either
 * contained in a {@link FileListActivity} in two-pane mode (on tablets) or a
 * {@link FileDetailActivity} on handsets.
 */
public class FileDetailFragment extends Fragment implements myActivtyInterface {
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String csvExt = "csv";

	public static final int ACTIVITY_ID = 2;

	public static final int SHOW_PROGRESS = 0;
	public static final int SHOW_GRAPH = 1;
	public static final int SHOW_EMPTY = 2;

	private Handler h;
	private String currentCsv = null;

	public static final String ARG_ITEM_NAME = "ru.sctbelpa.meteostation_service.name";
	public static final String DIRECTORY_NAME = "ru.sctbelpa.meteostation_service.Dir";
	private static final Pattern csvpattern = Pattern.compile(
			"(\\d*)\\.(\\d*);(\\d*\\.?,?\\d*);(\\d*\\.?,?\\d*);(\\d*\\.?,?\\d*);(\\d*\\.?,?\\d*)"); 

	private static final Pattern hader = Pattern.compile(
			//Date;P [mmHg];T [��C];Fp [Hz];Ft [Hz]
			"Date;P \\[mmHg\\];T \\[.*C\\];Fp \\[Hz\\];Ft \\[Hz\\]");

	/**
	 * The dummy content this fragment is presenting.
	 */
	private FSContent.FsObject mItem;
	private View rootView;

	private LinearLayout ProgressLayout;
	private LinearLayout EmptyMsglayout;
	private LinearLayout GraphLayout;

	private View.OnClickListener saveCsv = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			ExportCoeffsDialog fileDialog = new ExportCoeffsDialog(FileDetailFragment.this.getContext());
			fileDialog.setExtFilter(csvExt);
			fileDialog.setOpenDialogListener(new OpenFileDialog.OpenDialogListener() {

				@Override
				public void OnSelectedFile(String fileName) {
					File f = new File(fileName);

					FileOutputStream s = null;
					try {
						s = new FileOutputStream(f);
						s.write(currentCsv.getBytes());
						s.write('\n');

						Toast.makeText(getContext(), 
								R.string.write_csv_file_success,
								Toast.LENGTH_SHORT).show();
					} catch (IOException e) {
						Toast.makeText(getContext(), 
								getResources().getString(R.string.csv_export_fail) + 
								"(" + e.getMessage() + ")",
								Toast.LENGTH_SHORT).show();
					} 
					if (s != null)
						try {
							s.close();
						} catch (IOException e) {}
				}
			});
			fileDialog.show();
		}
	};

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public FileDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		h = new Handler();
		Bundle arguments = getArguments();

		if (arguments.containsKey(ARG_ITEM_NAME)) {
			mItem = FSContent.ITEM_MAP.get(arguments.getString(
					ARG_ITEM_NAME));

			R4_2App.execCommand(R4_2App.buildCommand(
					R4_2App.CMD_CAT, FileDetailFragment.this, mItem.name));
		}

	}

	public List<Dataset> parseCSV(List<String> csv)
	{
		// celsium degree parce trubble
		//final String header = "Date;P [mmHg];T [°C];Fp [Hz];Ft [Hz]";

		List<Dataset> result = new ArrayList<Dataset>();
		try
		{
			Matcher hm = hader.matcher(csv.get(0));
			if (hm.matches())
			{
				csv.remove(0);
				for (String line : csv) {
					Matcher m = csvpattern.matcher(line);
					if (m.find())
					{
						Date timestamp = new Date();
						timestamp.setMinutes(Integer.parseInt(m.group(1)));
						timestamp.setSeconds(Integer.parseInt(m.group(2)));

						Dataset item = new Dataset(
								timestamp.getTime(),
								Float.parseFloat(m.group(3)),
								Float.parseFloat(m.group(4))
								);
						result.add(item);
					}
				}
			}
		}
		catch (Exception e)
		{
			Log.e("CSV parser", e.getMessage());
		}

		return result;
	}

	public void SetData(String csv)
	{
		currentCsv = csv;

		List<String> csvStrings = new LinkedList<String>();
		for (String s : csv.split("\n"))
			csvStrings.add(s.trim());

		List<Dataset> graphdata = parseCSV(csvStrings);
		if (!graphdata.isEmpty())
			DrawGraph(graphdata);
		else
			SwitchViewMode(SHOW_EMPTY);
	}

	public void DrawGraph(List<Dataset> dataset)
	{
		GraphLayout.addView(graph.buildGraph(dataset, ""));

		SwitchViewMode(SHOW_GRAPH);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_file_detail,
				container, false);

		// Show the dummy content as text in a TextView.
		if (mItem != null) 
		{
			((TextView) rootView.findViewById(R.id.Filename_graph))
			.setText(mItem.name);

			ProgressLayout = (LinearLayout)rootView.findViewById(R.id.loading_file_layout);
			EmptyMsglayout = (LinearLayout)rootView.findViewById(R.id.empty_file_layout);
			GraphLayout = (LinearLayout)rootView.findViewById(R.id.graph_history);

			rootView.findViewById(R.id.saveValued_cmd).setOnClickListener(saveCsv);

			SwitchViewMode(SHOW_PROGRESS);
		}
		return rootView;
	}

	public void SwitchViewMode(int mode)
	{
		ProgressLayout.setVisibility(View.GONE);
		EmptyMsglayout.setVisibility(View.GONE);
		GraphLayout.setVisibility(View.GONE);

		switch (mode) {
		case SHOW_PROGRESS:
			ProgressLayout.setVisibility(View.VISIBLE);
			currentCsv = null;
			break;
		case SHOW_GRAPH:
			GraphLayout.setVisibility(View.VISIBLE);
			break;
		case SHOW_EMPTY:
			currentCsv = null;
			EmptyMsglayout.setVisibility(View.VISIBLE);
			break;
		}
	}

	@Override
	public Handler getHandler() {
		return h;
	}

	@Override
	public Integer getID() {
		return ACTIVITY_ID;
	}

	@Override
	public Context getContext() {
		// TODO Auto-generated method stub
		return getActivity();
	}
}
