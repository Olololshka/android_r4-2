package ru.sctbelpa.r4_2.FsModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.sctbelpa.r4_2.R;
import ru.sctbelpa.r4_2.adepters.FileAdepter;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class FSContent {

	/**
	 * An array of sample (dummy) items.
	 */
	public static List<FsObject> ITEMS = new ArrayList<FsObject>();

	/**
	 * A map of sample (dummy) items, by ID.
	 */
	public static Map<String, FsObject> ITEM_MAP = new HashMap<String, FsObject>();
	
	public static FileAdepter adeptor;

	public static void addItem(FsObject item) {
		ITEMS.add(item);
		ITEM_MAP.put(item.name, item);
	}
	
	public static void addItems(List<FsObject> items) {
		for (FsObject item : items)
			addItem(item);
	}

	public static void clearItems()
	{
		ITEMS.clear();
		ITEM_MAP.clear();
	}

	/**
	 * A dummy item representing a piece of content.
	 */
	public static class FsObject {
		public String name;
		public boolean isFolder;

		public FsObject(String name) {
			if (name.endsWith("/"))
			{
				this.name = name.substring(0, name.length() - 1); // remove "/" at start
				this.isFolder = true;
			}
			else
			{
				this.name = name;
				this.isFolder = false;
			}
		}

		@Override
		public String toString() {
			return name;
		}

		public int IconId()
		{
			if (isFolder)
			{
				if (name.equals(".."))
					return R.drawable.catalogup;
				else
					return R.drawable.catalog;
			}
			else 
				return R.drawable.data128;
		}
	}
}
