package ru.sctbelpa.r4_2.halpers;

import java.util.concurrent.Future;

import ru.sctbelpa.r4_2.R4_2App;

public class Pinger {

	private static Future<?> pingerctrl; 

	public static void StartPinger()
	{
		if (pingerctrl != null)
			StopPinger();
		
		pingerctrl = R4_2App.scheduleAtFixedRate(
				R4_2App.buildCommand(R4_2App.CMD_PING, null, null), 2000);
	}

	public static void StopPinger()
	{
		if (pingerctrl != null)
		{
			pingerctrl.cancel(true);
			pingerctrl = null;
		}
	}
}
