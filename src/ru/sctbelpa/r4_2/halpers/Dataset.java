package ru.sctbelpa.r4_2.halpers;

public class Dataset 
{
	public final long timestamp;
	public final float T;
	public final float P;

	public Dataset() 
	{
		this.timestamp = 0;
		this.T = 0;
		this.P = 0;
	}
	
	public Dataset(long timestamp, float T, float P)
	{
		this.timestamp = timestamp;
		this.T = T;
		this.P = P;
	}
};