package ru.sctbelpa.r4_2.halpers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.chart.LineChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import ru.sctbelpa.r4_2.R;
import ru.sctbelpa.r4_2.R4_2App;
import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.view.View;

public class graph {
	@SuppressLint("SimpleDateFormat")
	public static final SimpleDateFormat DATEFORMAT =  new SimpleDateFormat("mm:ss");
	private static final int maxXLabels = 10;
	private static PointStyle[] styles = new PointStyle[] {
		PointStyle.CIRCLE, PointStyle.CIRCLE};

	private static XYMultipleSeriesDataset prepareDataset()
	{
		Resources res = R4_2App.getAppContext().getResources();
		String [] setiesNames = new String[] {
				res.getString(R.string.graph_Y1),
				res.getString(R.string.graph_Y2)
				};
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		for (int i = 0; i < setiesNames.length; ++i)
		{
			XYSeries series = new XYSeries(setiesNames[i], i);
			dataset.addSeries(series);
		}
		return dataset;
	}
	
	private static XYMultipleSeriesRenderer buildRenderer(int[] colors, PointStyle[] styles) {
		XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer(colors.length);
		renderer.setAxisTitleTextSize(16);
		renderer.setChartTitleTextSize(20);
		renderer.setLabelsTextSize(15);
		renderer.setLegendTextSize(15);
		renderer.setPointSize(5f);
		renderer.setMargins(new int[] { 20, 30, 15, 20 });
		int length = colors.length;
		for (int i = 0; i < length; i++) {
			XYSeriesRenderer r = new XYSeriesRenderer();
			r.setColor(colors[i]);
			r.setPointStyle(styles[i]);
			r.setFillPoints(true);
			r.setLineWidth(3.0f);
			renderer.addSeriesRenderer(r);
		}
		return renderer;
	}
	
	private static void configureRender(XYMultipleSeriesRenderer render)
	{
		render.setXLabels(0);
		
		render.setShowLegend(false);
		render.setXLabelsAngle(-45);
		render.setXLabelsAlign(Align.RIGHT);
		//render.setBarSpacing(0.9); // 
		render.setApplyBackgroundColor(true);
		render.setBackgroundColor(Color.TRANSPARENT);
		render.setMarginsColor(Color.TRANSPARENT);
		//render.setZoomEnabled(true, true);//(true, false);
		
		render.setYAxisAlign(Align.LEFT, 0); //h
		render.setYAxisAlign(Align.RIGHT, 1); //T
		render.setYLabelsAlign(Align.RIGHT, 0);
		render.setYLabelsAlign(Align.LEFT, 1);
		
		render.setShowGridY(true);
		render.setGridColor(Color.GRAY);

		//render.setYAxisMin(0, 0);
		//render.setYAxisMax(100, 0);
		//render.setBarSpacing(4);

		render.setPointSize(4.5f);
		
		render.setPanEnabled(false); // disable scroll
		render.setZoomEnabled(false, false); // disable zoom
	}
	
	private static void setChartSettings(XYMultipleSeriesRenderer renderer,
			String title,
			int axesColor,
			int labelsColor) 
	{
		renderer.setChartTitle(title);
		renderer.setAxesColor(axesColor);
		renderer.setLabelsColor(labelsColor);
	}
		
	public static View buildGraph(List<Dataset> d, String title)
	{
		XYMultipleSeriesDataset dataset = prepareDataset();

		Resources res = R4_2App.getAppContext().getResources();
		final int [] colors = new int[] { 
				res.getColor(R.color.perssure_chart_color),
				res.getColor(R.color.temperature_chart_color)
				};	

		XYMultipleSeriesRenderer render = buildRenderer(colors, styles);

		int xLabelRedictionDevider;
		if (d.size() > maxXLabels)
			xLabelRedictionDevider = d.size() / maxXLabels;
		else 
			xLabelRedictionDevider = 1;
		
		int i = 0;
		for (Dataset item : d) {
			long t = item.timestamp;
			if (i % xLabelRedictionDevider == 0)
			{
				Date date = new Date(t);
				render.addXTextLabel(t, DATEFORMAT.format(date));
			}
			else 
				render.addXTextLabel(t, "");
			
			((XYSeries)dataset.getSeries()[0]).add(t, item.P);
			((XYSeries)dataset.getSeries()[1]).add(t, item.T);
			++i;
		}

		setChartSettings(render, title, Color.LTGRAY, Color.WHITE);
		configureRender(render);
/*
		FillOutsideLine fill = new FillOutsideLine(FillOutsideLine.Type.BELOW);
		fill.setColor(res.getColor(R.color.temperature_underchart_color));
		((XYSeriesRenderer)render.getSeriesRendererAt(0)).addFillOutsideLine(fill);
		
		fill = new FillOutsideLine(FillOutsideLine.Type.BELOW);
		fill.setColor(res.getColor(R.color.perssure_underchart_color));
		((XYSeriesRenderer)render.getSeriesRendererAt(1)).addFillOutsideLine(fill);
	*/	
		return ChartFactory.getCombinedXYChartView(R4_2App.getAppContext(), 
				dataset, render, new String[] {LineChart.TYPE, LineChart.TYPE});
	}
}
