package ru.sctbelpa.r4_2.halpers;

import java.io.IOException;
import java.text.ParseException;

public interface CoefficientFileInterface {
	void Import() throws IOException, ParseException;
	void Export() throws IOException;
}
