package ru.sctbelpa.r4_2.coefficient;

import java.io.InvalidClassException;
import java.text.ParseException;

import ru.sctbelpa.r4_2.R4_2App;
import ru.sctbelpa.r4_2.commands.Command;

public class FloatCoefficient implements Coefficient {

	private final String name;
	private float value;
	int addr = 0;

	public FloatCoefficient(String name, float value) {
		this.name = name;
		this.value = value;
	}

	public FloatCoefficient(int name_resid, float value) {
		name = R4_2App.getAppContext().getResources().getString(name_resid);
		this.value = value;
	}

	public FloatCoefficient(String name, String value_hex) {
		this.name = name;
		value = Command.hex2float(value_hex);
	}

	public FloatCoefficient(int name_resid, String value_hex) {
		name = R4_2App.getAppContext().getResources().getString(name_resid);
		value = Command.hex2float(value_hex);
	}

	@Override
	public String getUnits() {
		return "";
	}

	@Override
	public int getType() {
		return TYPE_FLOAT;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	@Override
	public void setValueFromString(String value) throws ParseException {
		try {
			this.value = Float.parseFloat(value);
		} catch (NumberFormatException e) {
			throw new ParseException(e.getMessage(), 0);
		}
	}
	
	// http://stackoverflow.com/questions/14671594/java-print-four-byte-hexadecimal-number
	private static String hex(int n) {
	    // call toUpperCase() if that's required
	    return String.format("%8s", Integer.toHexString(n)).replace(" ", "");
	}

	@Override
	public String formatOutput() {
		return hex(Float.floatToRawIntBits(value));
	}

	@Override
	public Object valueObj() {
		return Float.valueOf(value);
	}

	@Override
	public void setValueObj(Object newval) throws InvalidClassException {
		if (newval instanceof Float)
			value = ((Float) newval).floatValue();
		else 
			throw new InvalidClassException("newval must be instance of Float");
	}

	@Override
	public int getAddress() {
		return addr;
	}

	@Override
	public void setAddress(int addr) {
		this.addr = addr;
	}
}
