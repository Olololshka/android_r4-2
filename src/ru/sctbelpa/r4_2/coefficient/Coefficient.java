package ru.sctbelpa.r4_2.coefficient;

import java.io.InvalidClassException;
import java.text.ParseException;

public interface Coefficient {
	
	public static final int TYPE_FLOAT = 1;
	public static final int TYPE_INT = 2;
	
	public String getUnits();
	public int getType();
	public String getName();
	public void setValueFromString(String value) throws ParseException;
	public String formatOutput();
	public Object valueObj();
	public void setValueObj(Object newval) throws InvalidClassException;
	public int getAddress();
	public void setAddress(int addr);
}
