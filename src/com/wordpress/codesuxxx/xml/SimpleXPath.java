package com.wordpress.codesuxxx.xml;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * @author Владелец
 * почти целиком взято http://stepa.name/archives/17
 * 
 * https://codesuxxx.wordpress.com/2011/04/06/парсинг-xml-файла-на-androiddom/
 */
public class SimpleXPath {
	public static final String XPATH_SEPARATOR = "/";

	Document mDocument;

	/**
	 * @author Владелец
	 * Узел xml-файла.
	 */
	public class XNode {
		Node mNode;

		/**
		 * @param node
		 * узел xml-файла
		 */
		public XNode(Node node) {
			mNode = node;
		}

		/**
		 * @param path
		 * Относительный путь к вложенному узлу. 
		 * @return Вложенный узел.
		 * @throws XNodeException
		 * Если вложенный узел по данному пути отсутствует, то возбуждается исключение.
		 */
		public XNode getChild(String path) throws XNodeException {
			List<XNode> nodes = getXPathNodes(mNode, path);

			if (nodes.size() == 0) {
				String message = String.format("Node [%s] has no child at path [%s]!", mNode.getNodeName(), path);

				throw new XNodeException(message);
			}

			return nodes.get(0);
		}

		/**
		 * @param path
		 * Относительный путь к массиву вложенных узлов. 
		 * @return Массив вложенных узлов.
		 * @throws XNodeException
		 * Если нет вложенных узлов по данному пути, то возбуждается исключение.
		 */
		public List<XNode> getChildren(String path) throws XNodeException {
			List<XNode> result = getXPathNodes(mNode, path);

			if (result.isEmpty()) {
				String message = String.format("Node [%s] has no children at path [%s]!", mNode.getNodeName(), path);

				throw new XNodeException(message);
			}

			return result;
		}

		/**
		 * Получить имя узла.
		 * @return Имя узла.
		 */
		public String getName() {
			return mNode.getNodeName();
		}

		/**
		 * Получить текстовое значение узла.
		 * @return Текстовое значение узла.
		 */
		public String getValue() {
			return mNode.getChildNodes().item(0).getNodeValue();
		}

		/**
		 * Получить по имени атрибут узла.
		 * @param name Имя атрибута.
		 * @return Значение атрибута.
		 * @throws XNodeException
		 * Если атрибут с данным именем не найден, то возбуждается исключение.
		 */
		String getAttribute(String name) throws XNodeException {
			Element e = (Element) mNode;
			String attribute = e.getAttribute(name);

			if (attribute.length() == 0) {
				String message = String.format("Node [%s] has no [%s] attribute!",
						getName(), name);

				throw new XNodeException(message);
			}

			return attribute;
		}	

		/**
		 * Получить строковый атрибут.
		 * @param name Имя атрибута.
		 * @return Атрибут.
		 * @throws XNodeException
		 * Если атрибут с данным именем не найден, то возбуждается исключение.
		 */
		public String getStringAttribute(String name) throws XNodeException {
			return getAttribute(name);
		}

		/**
		 * Получить целочисленный атрибут.
		 * @param name Имя атрибута.
		 * @return Атрибут.
		 * @throws XNodeException
		 * Если атрибут с данным именем не найден, то возбуждается исключение.
		 */
		public int getIntAttribute(String name) throws XNodeException {
			return Integer.parseInt(getAttribute(name));
		}

		/**
		 * Получить числовой атрибут.
		 * @param name Имя атрибута.
		 * @return Числовой атрибут.
		 * @throws XNodeException
		 * Если атрибут с данным именем не найден, то возбуждается исключение.
		 */
		float getFloatAttribute(String name) throws XNodeException {
			return Float.parseFloat(getAttribute(name));
		}
	}

	/**
	 * @param view View содержащий ресурсы.
	 * @param xmlId Идентификатор ресурса xml-файла.
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * Если формат xml-файла неправильный, то могут возбуждаться исключения.
	 */
	public SimpleXPath(InputStream is) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

		mDocument = dBuilder.parse(is);
	}

	/**
	 * Получить корневой узел.
	 * @return Корневой узел.
	 */
	public Node getRoot() {
		return mDocument;
	}

	/**
	 * Получить узел по относительному пути. 
	 * @param root Узел с которго начинается поиск.
	 * @param path Относительный путь.
	 * @return Узел по относительному пути или null.
	 * Если узел по относительному пути не будет найден, то возбуждается исключение.
	 */
	public XNode getXPathNode(Node root, String path) {
		XNode result = null;
		String[] node = path.split(XPATH_SEPARATOR, 2);
		boolean isFinishing = node.length == 1;
		Node n = root.getFirstChild();

		while (n != null) {
			if (node[0].equals(n.getNodeName())) {
				if (isFinishing) {
					result = new XNode(n);
				} else {
					result = getXPathNode(n, node[1]);
				}

				break;
			}
			try {
				n = n.getNextSibling();
			} catch (Exception e) {
				n = null;
			}
		}

		return result;
	}

	/**
	 * Получить массив узлов по относительному пути.
	 * @param root Узел с которго начинается поиск.
	 * @param path Относительный путь.
	 * @return Массив узлов.
	 */
	public List<XNode> getXPathNodes(Node root, String path) {
		List<XNode> result = new ArrayList<XNode>();

		getNodes(root, path, result);

		return result;
	}

	/**
	 * Получить массив узлов по относительному пути.
	 * Функция вызывается рекурсивно, каждый раз убирая из пути первую часть до разделителя XPATH_SEPARATOR. 
	 * @param root Узел с которго начинается поиск.
	 * @param path Относительный путь.
	 * @param result Массив для хранения результата.
	 */
	private void getNodes(Node root, String path, List<XNode> result) {
		String[] node = path.split(XPATH_SEPARATOR, 2);
		boolean isFinishing = node.length == 1;
		Node n = root.getFirstChild();

		while (n != null) {
			if (node[0].equals(n.getNodeName())) {
				if (isFinishing) {
					result.add(new XNode(n));
				} else {
					getNodes(n, node[1], result);
				}
			}
			try {
				n = n.getNextSibling();
			} catch (Exception e) {
				n = null;
			}
		}
	}
}