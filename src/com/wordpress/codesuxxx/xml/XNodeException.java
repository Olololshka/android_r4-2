package com.wordpress.codesuxxx.xml;

public class XNodeException extends Exception {

	/**
	 * Some serialization stuff.
	 */
	private static final long serialVersionUID = 6194649056119743851L;

	public XNodeException() {
		super();
	}

	public XNodeException(String message) {
		super(message);
	}
}